package msandro.msutilities.handler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.stream.Collectors;

import net.minecraftforge.common.DimensionManager;


public class PlayerDataHandler {
	
	public static void writeFile(UUID uuid, String[] content) throws IOException {
		String dir =  DimensionManager.getCurrentSaveRootDirectory()+"\\playerdata\\" +uuid +".msutils";
		
		String text = "";
		for (int i = 0; i < content.length; i++) {
			text = text + content[i] + String.format("\n");
		}
		
		Files.write(Paths.get(dir), text.getBytes());
	}
	
	
	public static String[] readFile(UUID uuid) {
		String[] output = null;
		try {
			String dir =  DimensionManager.getCurrentSaveRootDirectory()+"\\playerdata\\" +uuid +".msutils";
			String file = Files.lines(Paths.get(dir)).collect(Collectors.joining("\n"));
			output = file.split("\n");
		} catch (IOException e) {
			System.out.println("error on reading file");
		}
		return output;
	}
	
	public static String getValue(UUID uuid, int line) {
		String output = null;
		String[] input = readFile(uuid);
		output = input[line].split(":")[1];
		return output;
	}
	
	public static void updateFile(UUID uuid, int line, String arg) throws IOException {
		try {
			String[] playerData = readFile(uuid);
			playerData[line] = playerData[line].split(":")[0] + ":" + arg;
			writeFile(uuid, playerData);
			System.out.println("Updated file");
		} catch (IOException e) {
			System.out.println("error on updating file");
		}
	}
}
