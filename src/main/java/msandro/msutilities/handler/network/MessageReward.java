package msandro.msutilities.handler.network;

import javax.swing.plaf.basic.BasicComboBoxUI.ItemHandler;

import io.netty.buffer.ByteBuf;
import msandro.msutilities.manager.ConfigManager;
import msandro.msutilities.manager.ItemManager;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class MessageReward extends MessageBase<MessageReward> {

	private int kit;
	
	public MessageReward(){}
	
	public MessageReward(int kit) {
		this.kit = kit;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		kit = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(kit);		
	}

	@Override
	public void handleClientSide(MessageReward message, EntityPlayer player) {
	}

	@Override
	public void handleServerSide(MessageReward message, EntityPlayer Player) {
		String[] list = null;
		
		switch ((int) message.kit) {
		case 1:
			list = ConfigManager.easyItems;
			break;
		case 2:
			list = ConfigManager.normalItems;
			break;
		case 3:
			list = ConfigManager.hardItems;
			break;
		}
		
		for(int i = 0; i < list.length; i++) {
			
			if (list[i].split(";").length == 2) {
				String item = list[i].split(";")[0];
				String[] meta = item.split(":");
				int stack = Integer.valueOf(list[i].split(";")[1]);
				((EntityPlayer) Player).inventory.addItemStackToInventory(new ItemStack(Item.getByNameOrId(meta[0]+":"+meta[1]), stack, Integer.valueOf(meta[2])));
			} else {
				System.out.println("Config file: Items: WRONG INPUT");
			}
		}
		
		//PlayerDataHandler.updateFile(Player.getUniqueID(), 7, "true");
		
		Player.inventory.clearMatchingItems(ItemManager.guideitem, -1, -1, null);
	}

}
