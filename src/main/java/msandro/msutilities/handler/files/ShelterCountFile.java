package msandro.msutilities.handler.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ShelterCountFile {
	public static void writeFile(String path, String value) throws IOException {
		String dir =  path+"\\ShelterCount.msutils";		
		Files.write(Paths.get(dir), value.getBytes());
	}
	
	
	public static int readFile(String path) throws IOException {
		int output = 0;
		String dir = path+"\\ShelterCount.msutils";
		
		File f = new File(dir);
		if(f.exists()) {
			try {
				BufferedReader brTest = new BufferedReader(new FileReader(dir));
				output = Integer.valueOf(brTest.readLine());
				brTest.close();
				System.out.println("ShelterCount="+output);
			} catch (IOException e) {
				System.out.println("error on reading file");
			}
			return output;
		} else {
			writeFile(path, "0");
			output = 0;
		}
		return output;
	}
}
