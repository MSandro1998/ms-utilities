package msandro.msutilities.handler;

import java.awt.Toolkit;

import msandro.msutilities.manager.ConfigManager;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EventHandlerClient {
	public static boolean played = false;
	
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onGuiOpen(GuiOpenEvent event) {
		if (((event.getGui() instanceof GuiMainMenu)) && (!played)) {
			played = true;
			
			if (ConfigManager.startBeep) {
				Toolkit.getDefaultToolkit().beep();
			}
	    }
	}	
}
