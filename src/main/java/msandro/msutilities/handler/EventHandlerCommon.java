package msandro.msutilities.handler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import msandro.msutilities.VarList;
import msandro.msutilities.handler.files.PlayerDataHandler;
import msandro.msutilities.manager.ConfigManager;
import msandro.msutilities.structure.shelter;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class EventHandlerCommon {
	
	@SubscribeEvent
	public void onPlayerLogIn(PlayerLoggedInEvent event) throws IOException, InterruptedException {
		EntityPlayer Player = event.player;
		
		if (VarList.Side == "SERVER" || Minecraft.getMinecraft().isIntegratedServerRunning()) {
			String dir =  DimensionManager.getCurrentSaveRootDirectory().getPath(); 	
			
			List<String> results = new ArrayList<String>();
			File[] files = new File(dir +"\\playerdata").listFiles();
			
			//get player UUIDs
			for (int i = 0; i < files.length; i++) {
			    if (files[i].isFile()) {results.add(files[i].getName());}
			}
			
			//if player UUID doesn't exist
			if (!(results.contains(Player.getUniqueID() +".dat"))) {
				Player.inventory.clear();
				Player.setHealth(Player.getMaxHealth());
				
				int PosX = 0;
				int PosY = ConfigManager.ShelterHeight-1;
				int PosZ = 0;
				
	
				//Shelter
				if (ConfigManager.enableShelter) {
					if (ConfigManager.ShelterRandomPos) {
						int maximum = ConfigManager.ShelterRange;
						int minimum = ConfigManager.ShelterRange *-1;
						PosX = new Random().nextInt(maximum - minimum + 1) + minimum;
						PosZ = new Random().nextInt(maximum - minimum + 1) + minimum;
					} else {
						switch((VarList.ShelterCount+4) % 4) {
						case 0:
							PosX = VarList.ShelterCount *ConfigManager.ShelterRange;
							PosZ = 0;
							break;
						case 1:
							PosX = (VarList.ShelterCount *ConfigManager.ShelterRange)*(-1);
							PosZ = 0;
							break;	
						case 2:
							PosX = 0;
							PosZ = VarList.ShelterCount *ConfigManager.ShelterRange;
							break;
						case 3:
							PosX = 0;
							PosZ = (VarList.ShelterCount *ConfigManager.ShelterRange)*(-1);
							break;	
						}
					}
				} else {
					PosX = Player.getPosition().getX();
					PosY = Player.getPosition().getY();
					PosZ = Player.getPosition().getZ();
				}
				
				shelter.generateShelter(PosX, PosY, PosZ);
				if (ConfigManager.enableShelter) {
					Player.setPositionAndUpdate(PosX+6.5, ConfigManager.ShelterHeight, PosZ+0.5);
				}
				((EntityPlayer) Player).setSpawnChunk(Player.getPosition(), true, 0);
				
				String[] write = new String[8];
				write[0] =("SpawnPosX:"+Player.getPosition().getX());
				write[1] =("SpawnPosY:"+Player.getPosition().getY());
				write[2] =("SpawnPosZ:"+Player.getPosition().getZ());
				write[3] =("ShelterPosX:"+PosX);
				write[4] =("ShelterPosY:"+PosY);
				write[5] =("ShelterPosZ:"+PosZ);
				write[6] =("onSurface:false");
				write[7] =("setup:false");
				PlayerDataHandler.writeFile(Player.getUniqueID(), write);
			}		
		}
	}
	
	/*@SubscribeEvent
	public void onChat(ServerChatEvent event) {
		EntityPlayerMP Player = event.getPlayer();
		shelter.generateShelter(Player.getPosition().getX(), Player.getPosition().getY(), Player.getPosition().getZ());
	}*/
	
	//run on death
	@SubscribeEvent
	public void onDeath(PlayerDropsEvent event) throws IOException {
		if (ConfigManager.reachSurface && ConfigManager.enableShelter) {
			EntityPlayer Player = event.getEntityPlayer();
			
			String[] playerData = PlayerDataHandler.readFile(Player.getUniqueID());
			int PosX = Integer.valueOf(playerData[3].split(":")[1]);
			int PosY = Integer.valueOf(playerData[4].split(":")[1]);
			int PosZ = Integer.valueOf(playerData[5].split(":")[1]);
			int SpawnY = Integer.valueOf(playerData[1].split(":")[1]);
			String onSurface = playerData[6].split(":")[1];

			if ((Player.getPosition().getY() -SpawnY) <= 40 && onSurface.equals("false")) {
				System.out.println("Player died in the underground.");
				
				event.getDrops().clear();
				
				shelter.generateShelter(PosX, PosY, PosZ);
				Player.setPositionAndUpdate(PosX+6.5, ConfigManager.ShelterHeight, PosZ+0.5);
				((EntityPlayer) Player).setSpawnChunk(Player.getPosition(), true, 0);
			} else {
				System.out.println("Player died on surface");
				PlayerDataHandler.updateFile(Player.getUniqueID(), 6, "true");
			}
		}
	}
}
