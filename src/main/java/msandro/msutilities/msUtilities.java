package msandro.msutilities;

import msandro.msutilities.command.CommandHeal;
import msandro.msutilities.handler.CraftingHandler;
import msandro.msutilities.handler.EventHandlerClient;
import msandro.msutilities.handler.EventHandlerCommon;
import msandro.msutilities.handler.TooltipEventHandler;
import msandro.msutilities.handler.network.NetworkHandler;
import msandro.msutilities.manager.BlockManager;
import msandro.msutilities.manager.ConfigManager;
import msandro.msutilities.manager.GuiManager;
import msandro.msutilities.manager.ItemManager;
import msandro.msutilities.proxy.CommonProxy;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = msUtilities.MODID, version = msUtilities.VERSION, name = msUtilities.NAME)
public class msUtilities
{
    public static final String MODID = "msutilities";
    public static final String VERSION = "0.8.9";
    public static final String NAME = "MS Utilities";
    
    @Instance(MODID)
    public static msUtilities instance = new msUtilities();
    
    @SidedProxy(modId = MODID, serverSide = "msandro.msutilities.proxy.CommonProxy", clientSide = "msandro.msutilities.proxy.ClientProxy")
    public static CommonProxy proxy = new CommonProxy(); 
    
    public ItemManager items;
    public BlockManager blocks;  
    public CraftingHandler crafting;
    public ConfigManager config;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	NetworkHandler.init();    	
    	
    	items = new ItemManager();
    	items.init();
    	items.register();
    	
    	blocks = new BlockManager();
    	blocks.init();
    	blocks.register();
    	
		config = new ConfigManager(event.getSuggestedConfigurationFile());
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiManager());
		crafting = new CraftingHandler();
		crafting.register();
		
		MinecraftForge.EVENT_BUS.register(new EventHandlerCommon());
		
		if (event.getSide() == Side.CLIENT) {
			OBJLoader.INSTANCE.addDomain("msandro");
			MinecraftForge.EVENT_BUS.register(new EventHandlerClient());
			VarList.Side = "CLIENT";
		} else {
			VarList.Side = "SERVER";
		}
		
		if (ConfigManager.modTooltip) {
			if (Loader.isModLoaded("Waila")) {
				FMLLog.warning("Waila detected. It also adds the Mod Name to the Tooltip. Deactivating " + MODID + '.');
				return;
			}
			TooltipEventHandler tooltipEventHandler = new TooltipEventHandler();
			MinecraftForge.EVENT_BUS.register(tooltipEventHandler);
		}
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	proxy.registerModels();
    }
    
    @EventHandler
    public void serverLoad(FMLServerStartingEvent event) {
    	if (ConfigManager.healCommand) {
    		event.registerServerCommand(new CommandHeal());
    	}
    }
}
