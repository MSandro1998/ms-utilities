package msandro.msutilities.item;

import java.util.List;

import msandro.msutilities.msUtilities;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class GuideItem extends Item {
	
	public GuideItem() {
		super();
		this.setMaxStackSize(1);
		this.setMaxDamage(1);
		this.setNoRepair();
	}
	
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
		tooltip.add("quests, rewards and more!");
		super.addInformation(stack, playerIn, tooltip, advanced);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn, EnumHand hand) {
		playerIn.openGui(msUtilities.instance, 1, worldIn, playerIn.getPosition().getX(), playerIn.getPosition().getY(), playerIn.getPosition().getZ());
		
		return new ActionResult(EnumActionResult.SUCCESS, itemStackIn);
	}
	
}
