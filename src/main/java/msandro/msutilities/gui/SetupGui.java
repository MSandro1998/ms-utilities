package msandro.msutilities.gui;

import java.io.IOException;

import msandro.msutilities.handler.network.MessageReward;
import msandro.msutilities.handler.network.NetworkHandler;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import net.minecraftforge.fml.client.config.GuiSlider;

public class SetupGui extends GuiScreen {
	
	GuiSlider slider;
	public static int result = 1;
	
	@Override
	public void initGui() {
		buttonList.add(new GuiButtonExt(1, width/2-75, 100, 150, 20, "I need little help"));
		buttonList.add(new GuiButtonExt(2, width/2-75, 130, 150, 20, "I need some help!"));
		buttonList.add(new GuiButtonExt(3, width/2-75, 160, 150, 20, "I need much help!"));
	}
	
	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}
	
	@Override
	public void actionPerformed(GuiButton button) throws IOException {	
		
		switch ((int)button.id) {
		case 1:
			System.out.println(1);
			NetworkHandler.sendToServer(new MessageReward(3));
			break;
		case 2:
			System.out.println(2);
			NetworkHandler.sendToServer(new MessageReward(2));
			break;
		case 3:
			System.out.println(3);
			NetworkHandler.sendToServer(new MessageReward(1));
			break;
		}
		
		this.mc.displayGuiScreen(null);
        if (this.mc.currentScreen == null)
            this.mc.setIngameFocus();
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		this.fontRendererObj.drawString("MS Utilities SETUP", width/2-50, 30, 0xffffff);
		this.fontRendererObj.drawString("How much help do you need?", width/2-70, 85, 0xffffff);		
		super.drawScreen(mouseX, mouseY, partialTicks);
	}

}
