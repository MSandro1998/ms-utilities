package msandro.msutilities.proxy;

import msandro.msutilities.manager.BlockManager;
import msandro.msutilities.manager.ItemManager;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;

public class ClientProxy extends CommonProxy {

	//Client Stuff
	public void registerModels() {
		registerModel(ItemManager.guideitem, 0, new ModelResourceLocation(ItemManager.guideitem.getRegistryName(), "inventory"));
		registerModel(BlockManager.testblock, 0, new ModelResourceLocation(BlockManager.testblock.getRegistryName(), "inventory"));
	}
	
	private void registerModel(Object obj, int meta, ModelResourceLocation loc) {
		Item item = null;
		if (obj instanceof Item) {
			item = (Item) obj;
		} else if (obj instanceof Block) {
			item = Item.getItemFromBlock((Block)obj);
		} else {
			throw new IllegalArgumentException();
		}
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, meta, loc);
	}
}
