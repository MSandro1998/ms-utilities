package msandro.msutilities.manager;

import java.io.File;

import net.minecraftforge.common.config.Configuration;


public class ConfigManager {
	
	public static Configuration cfg; 
	
	public static boolean reachSurface = false;
	
	public static boolean enableGuide = true;
	
	public static boolean startBeep = true;
	public static boolean modTooltip = true;
	public static boolean healCommand = true;
	
	public static boolean enableShelter = true;
	public static boolean ShelterRandomPos = false;
	public static int ShelterHeight = 20;
	public static int ShelterRange = 100;
	public static int ShelterGlassColor = 3;
	public static boolean ShelterEnableHiddenBlock = true;
	public static String ShelterHiddenBlock = "minecraft:lava";
	public static boolean ShelterEnableEntitySpawning = true;
	public static String ShelterLeftEntity = "Skeleton";
	public static String ShelterRightEntity = "Zombie";
	
	public static String[] startingItems;
	public static String[] easyItems;
	public static String[] normalItems;
	public static String[] hardItems;
	
	public ConfigManager(File suggestedConfigurationFile) {
		cfg = new Configuration(suggestedConfigurationFile);
		cfg.load();
		setup();
	}

	private void setup() {		
		cfg.addCustomCategoryComment("Checkpoint", "EXPERIMENTAL FEATURE");
		cfg.setCategoryComment("Checkpoint", "EXPERIMENTAL FEATURE");
		reachSurface = cfg.get("Checkpoint", "ReachSurface", false).getBoolean();
		
		cfg.addCustomCategoryComment("Guide", "item to offer quests and rewards");
		cfg.setCategoryComment("Guide", "item to offer quests and rewards");
		enableGuide = cfg.get("Guide", "EnableGuideItem", true).getBoolean();
		
		cfg.addCustomCategoryComment("Items", "separate the items with double spaces");
		cfg.setCategoryComment("Items", "separate the items with double spaces");
		startingItems = cfg.get("Items", "StartingItems", "msutilities:guideitem:0;1").getString().split("  ");
		easyItems = cfg.get("Items", "EasyKitItems", "minecraft:iron_sword:0;1  minecraft:potion:0;1  minecraft:potion:0;1  minecraft:potion:0;1  minecraft:baked_potato:0;16").getString().split("  ");
		normalItems = cfg.get("Items", "NormalKitItems", "minecraft:stone_sword:0;1  minecraft:potion:0;2  minecraft:potion:0;1  minecraft:baked_potato:0;8").getString().split("  ");
		hardItems = cfg.get("Items", "HardKitItems", "minecraft:wooden_sword:0;1  minecraft:potion:0;1  minecraft:baked_potato:0;4").getString().split("  ");
		
		cfg.addCustomCategoryComment("Shelter", "Do not use generate the shelter above y:60!");
		cfg.setCategoryComment("Shelter", "Do not use generate the shelter above y:60!");
		enableShelter = cfg.get("Shelter", "EnableShelter", true).getBoolean();
		ShelterRandomPos = cfg.get("Shelter", "ShelterRandomPositioning", false).getBoolean();
		ShelterHeight = cfg.get("Shelter", "ShelterSpawnHeight", 20).getInt();
		ShelterRange = cfg.get("Shelter", "ShelterDistance", 100).getInt();
		ShelterGlassColor = cfg.get("Shelter", "ShelterGlassColor", 3).getInt();
		ShelterEnableHiddenBlock = cfg.get("Shelter", "EnableHiddenBlock", true).getBoolean();
		ShelterHiddenBlock = cfg.get("Shelter", "HiddenBlock", "minecraft:lava").getString();
		ShelterEnableEntitySpawning = cfg.get("Shelter", "EnableHiddenBlock", true).getBoolean();
		ShelterLeftEntity = cfg.get("Shelter", "LeftEntityName", "Skeleton").getString();
		ShelterRightEntity = cfg.get("Shelter", "RightEntityName", "Zombie").getString();
		
		cfg.addCustomCategoryComment("Utilities", "utilities and commands");
		cfg.setCategoryComment("Utilities", "commands and more");
		startBeep = cfg.get("Utilities", "EnableBeepOnStart", true).getBoolean();
		modTooltip = cfg.get("Utilities", "ShowModNameTooltip", true).getBoolean();
		healCommand = cfg.get("Utilities", "HealCommands", true).getBoolean();
		
		cfg.save();
	}
	
}
