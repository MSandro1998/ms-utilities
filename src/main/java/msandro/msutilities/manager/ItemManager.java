package msandro.msutilities.manager;

import msandro.msutilities.item.GuideItem;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ItemManager {

	public static Item guideitem;
	
	public void init() {
		guideitem = new GuideItem().setCreativeTab(CreativeTabs.MISC);
		NameManager.setNames(guideitem, "guideitem");
	}

	public void register() {
		registerItem(guideitem);
	}

	
	private void registerItem(Item item) {
		GameRegistry.register(item);
	}
}
