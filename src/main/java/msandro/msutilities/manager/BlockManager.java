package msandro.msutilities.manager;

import msandro.msutilities.block.BlockTest;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockManager {

	public static Block testblock;
	
	public void init() {
		testblock = new BlockTest();
		NameManager.setNames(testblock, "testblock");
	}

	public void register() {
		registerBlock(testblock);
	}

	
	private void registerBlock(Block block) {
		GameRegistry.register(block);
		ItemBlock itemblock = new ItemBlock(block);
		itemblock.setUnlocalizedName(block.getUnlocalizedName()).setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock);
	}
	
}
